import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://54.166.240.59:8085/')

WebUI.setText(findTestObject('User is in Product page/input_user_username'), 'user')

WebUI.setEncryptedText(findTestObject('User is in Product page/input_user_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('User is in Product page/button_Login'))

WebUI.verifyElementText(findTestObject('User is in Product page/img_Garden'), image)

WebUI.verifyElementText(findTestObject('User is in Product page/h5_Garden_result'), productName)

WebUI.verifyElementText(findTestObject('User is in Product page/p_Garden_description_result'), productDescription)

WebUI.verifyElementText(findTestObject('User is in Product page/h6_priceGarden_result'), price)

WebUI.verifyElementText(findTestObject('User is in Product page/button_add to cart'), addToCartButton)

WebUI.verifyElementText(findTestObject('User is in Product page/img_Banana'), image)

WebUI.verifyElementText(findTestObject('User is in Product page/h5_Banana_result'), productName)

WebUI.verifyElementText(findTestObject('User is in Product page/p_Banana_description_result'), productDescription)

WebUI.verifyElementText(findTestObject('User is in Product page/h6_priceBanana_result'), price)

WebUI.verifyElementText(findTestObject('User is in Product page/button_add to cart'), addToCartButton)

WebUI.verifyElementText(findTestObject('User is in Product page/img_Orange'), image)

WebUI.verifyElementText(findTestObject('User is in Product page/h5_Orange_result'), productName)

WebUI.verifyElementText(findTestObject('User is in Product page/p_Orange_description_result'), productDescription)

WebUI.verifyElementText(findTestObject('User is in Product page/h6_priceOrange_result'), price)

WebUI.verifyElementText(findTestObject('User is in Product page/button_add to cart'), addToCartButton)

WebUI.verifyElementText(findTestObject('User is in Product page/img_Papaya'), image)

WebUI.verifyElementText(findTestObject('User is in Product page/h5_Papaya_result'), productName)

WebUI.verifyElementText(findTestObject('User is in Product page/p_Papaya_description_result'), productDescription)

WebUI.verifyElementText(findTestObject('User is in Product page/h6_pricePapaya_result'), price)

WebUI.verifyElementText(findTestObject('User is in Product page/button_add to cart'), addToCartButton)

WebUI.verifyElementText(findTestObject('User is in Product page/img_Rambutan'), image)

WebUI.verifyElementText(findTestObject('User is in Product page/h5_Rambutan_result'), productName)

WebUI.verifyElementText(findTestObject('User is in Product page/p_Rambutan_description_result'), productDescription)

WebUI.verifyElementText(findTestObject('User is in Product page/h6_priceRambutan_result'), price)

WebUI.verifyElementText(findTestObject('User is in Product page/button_add to cart'), addToCartButton)

WebUI.closeBrowser()

